var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

//sass setup
var sassMiddleware = require('node-sass-middleware');

// Setup SASS directories
var path = require('path');
app.use(sassMiddleware({
    src: __dirname + '/public', 
    dest: __dirname + '/public', 
    debug: true, 
    outputStyle: 'compressed'
  }),
  // The static middleware must come after the sass middleware
  express.static(path.join(__dirname, 'public'))
)
app.use(express.static(path.join(__dirname, 'apps')));
app.use(express.static(path.join(__dirname, '/node_modules/bootstrap/js/dist')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
