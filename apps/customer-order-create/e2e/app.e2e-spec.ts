import { CustomerOrderCreatePage } from './app.po';

describe('customer-order-create App', function() {
  let page: CustomerOrderCreatePage;

  beforeEach(() => {
    page = new CustomerOrderCreatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
