var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Porträt vom Foto. Die Geschnekidee mit Errinnerungswert' });
});

/* GET GTC page. */
router.get('/gtc', function(req, res) {
  res.render('gtc', { title: 'Allgemeine Geschäftsbedingungen' });
});

/* GET contact page. */
router.get('/contact', function(req, res) {
  res.render('contact', { title: 'Kontakt' });
});

/* GET imprint page. */
router.get('/imprint', function(req, res) {
  res.render('imprint', { title: 'Impressum' });
});

/* GET customer-order-create page. */
router.get('/customer-order-create', function(req, res) {
  res.render('customer-order-create', { title: 'Bestellen' });
});

module.exports = router;
